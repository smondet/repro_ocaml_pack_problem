

Build `lib_a`, base library:

```
cd lib_a
ocamlc -c mod_a.ml -o mod_a.cmo -for-pack lib_a
ocamlc -pack mod_a.cmo -o lib_a.cmo
cd ../
mkdir -p _install/lib_a
cp lib_a/lib_a.* _install/lib_a/
```

Build `lib_b`, using `-I ../lib_a` instead of `-I ../_install/lib_a` introduces
the problem (`mod_b.ml` can access the `Mod_a` module bypassing the `pack`):

```
cd lib_b
ocamlc -c -I ../lib_a/ mod_b.ml -o mod_b.cmo -for-pack lib_b
ocamlc -pack mod_b.cmo -o lib_b.cmo
cd ../
mkdir -p _install/lib_b
cp lib_b/lib_b.* _install/lib_b/
```

Build `lib_c`, only access properly `_install/lib_{a,b}`:

```
cd lib_c
ocamlc -c -I ../_install/lib_a/ -I ../_install/lib_b/ mod_c.ml -o mod_c.cmo -for-pack lib_c
cd ../
```

`mod_c.ml` builds OK but should not:

```ocaml
let f p  () = 
  (Lib_b.Mod_b.get_mod_a p)#product#path
```

the type `Lib_a.Mod_a.product` does not have a `path` method.

If we modify `mod_c.ml` slightly, we get closer to the expected behavior:

```ocaml
let f p  () = 
  let x = (Lib_b.Mod_b.get_mod_a p) in
  x#product#path
```

we get a type error but that cites the wrong method name:

```
File "mod_c.ml", line 6, characters 2-3:
Error: This expression has type Mod_a.product Lib_a.Mod_a.Container.t
       It has no method product
```
