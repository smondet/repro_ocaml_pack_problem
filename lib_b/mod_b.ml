
(*

*)

type 'a t = ..
type 'a t +=
  | Mod_a: Mod_a.product Lib_a.Mod_a.Container.t -> int t

let get_mod_a : type a . a t -> Mod_a.product Lib_a.Mod_a.Container.t =
  function
  | Mod_a l -> l
  | other -> failwith "not mod a"
