
module Container : sig
  type 'a product = 'a
    constraint 'a = < is_done : string option ; .. >
  type 'product t = <
    product : 'product product;
  >
  val make: 'a product -> 'a t 
end = struct
  type 'a product = 'a
    constraint 'a = < is_done : string option ; .. >
  type 'product t = <
    product : 'product product;
  >
  let make p = object method product = p end
end

(*


*)
type product = <
    is_done : string option ;
    class1_path : string;
    class2_path: string;
    work_dir_path: string >

let f () : product Container.t =
  Container.make (
    object
      method is_done = None
      method class1_path = "idjede"
      method class2_path = "idjede"
      method work_dir_path = "idjede"
    end
  )
